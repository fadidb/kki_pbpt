<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'c_home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


$route['list-pemesanan'] = 'c_home/listPesanan';
$route['list-supplier'] = 'c_home/list_supplier';
$route['list-barang'] = "c_home/list_barang";

$route['view'] = "c_print";
