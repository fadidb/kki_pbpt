<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Print_model extends CI_Model {
  public function view_by_date($date){
    $this->db->select('*');
    $this->db->from('kki_pemesanan');
    $this->db->join('kki_supplier', 'kki_pemesanan.KodeSupplier = kki_supplier.KodeSupplier');
    $this->db->join('kki_barang', 'kki_pemesanan.KodeBarang = kki_barang.KodeBarang');
    $this->db->where('DATE(TanggalPesanan)', $date); // Tambahkan where tanggal nya
        
    return $this->db->get()->result();// Tampilkan data transaksi sesuai tanggal yang diinput oleh user pada filter
  }
    
  public function view_by_month($month, $year){
    $this->db->select('*');
    $this->db->from('kki_pemesanan');
    $this->db->join('kki_supplier', 'kki_pemesanan.KodeSupplier = kki_supplier.KodeSupplier');
    $this->db->join('kki_barang', 'kki_pemesanan.KodeBarang = kki_barang.KodeBarang');
        $this->db->where('MONTH(TanggalPesanan)', $month); // Tambahkan where bulan
        $this->db->where('YEAR(TanggalPesanan)', $year); // Tambahkan where tahun
        
    return $this->db->get()->result(); // Tampilkan data transaksi sesuai bulan dan tahun yang diinput oleh user pada filter
  }
    
  public function view_by_year($year){
    $this->db->select('*');
    $this->db->from('kki_pemesanan');
    $this->db->join('kki_supplier', 'kki_pemesanan.KodeSupplier = kki_supplier.KodeSupplier');
    $this->db->join('kki_barang', 'kki_pemesanan.KodeBarang = kki_barang.KodeBarang');
        $this->db->where('YEAR(TanggalPesanan)', $year); // Tambahkan where tahun
        
    return $this->db->get()->result(); // Tampilkan data transaksi sesuai tahun yang diinput oleh user pada filter
  }
    
  public function view_all(){

    $this->db->select('*');
    $this->db->from('kki_pemesanan');
    $this->db->join('kki_supplier', 'kki_pemesanan.KodeSupplier = kki_supplier.KodeSupplier');
    $this->db->join('kki_barang', 'kki_pemesanan.KodeBarang = kki_barang.KodeBarang');

    // $query = $this->db->get();
    // return $query->result();
    return $this->db->get()->result();
    
    // return $this->db->get('kki_pemesanan')->result(); // Tampilkan semua data transaksi
  }
    
    public function option_tahun(){
        $this->db->select('YEAR(TanggalPesanan) AS tahun'); // Ambil Tahun dari field tgl
        $this->db->from('kki_pemesanan'); // select ke tabel transaksi
        $this->db->order_by('YEAR(TanggalPesanan)'); // Urutkan berdasarkan tahun secara Ascending (ASC)
        $this->db->group_by('YEAR(TanggalPesanan)'); // Group berdasarkan tahun pada field tgl
        
        return $this->db->get()->result(); // Ambil data pada tabel transaksi sesuai kondisi diatas
    }
}