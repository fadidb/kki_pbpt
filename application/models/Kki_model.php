<?php 

class Kki_model extends CI_Model{

    // DATA PEMESANAN
    public function get_listpemesanan(){
        return $this->db->get('kki_pemesanan')->result_array();
    }

    public function add_listpemesanan2() {
        // $namafile = $this->input->post('id');
        // $config['encrypt_name'] = TRUE;
		$config['upload_path'] = './assets/buktinota';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
		// $config['file_name']			= str_replace(' ', '_', $namafile);
		$config['max_size'] = '10000';
        $config['max_width']  = '';
        $config['max_height']  = '';
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('buktinota'))
		{

        $file_name_gambar = $_FILES["buktinota"]["name"];
        $file_nama_gambar_md5= md5($file_name_gambar) ;
		$file_ext_gambar = pathinfo($file_nama_gambar_md5, PATHINFO_EXTENSION);
        $file_gambar = str_replace(' ', '_', $file_name_gambar).'.'.strtolower($file_ext_gambar);
        // $foto_nota = md5($file_gambar) ;

		}
		// else
		// {
		// 	echo $this->upload->display_errors();die();
		// }
		$data = array (
			"NoPesanan" => $this->input->post('id'),
            "TanggalPesanan" => date('Y/m/d/ H:i:s', strtotime($this->input->post('tgl_pemesanan'))),
            "KodeSupplier" => $this->input->post('supplier'),
            "KodeBarang" => $this->input->post('barang'),
            "JumlahBarang" => $this->input->post('jumlah'),
            "buktinota" => $file_gambar
		);

		$this->db->insert('kki_pemesanan',$data);
    }




    public function edit_pesanan2($NoPesanan) {
       $NoPesanan = $this->input->post('NoPesanan');
       $data = array (
        "TanggalPesanan" => $this->input->post('tgl_pemesanan'),
        "KodeSupplier" => $this->input->post('supplier'),
        "KodeBarang" => $this->input->post('barang'),
        "JumlahBarang" => $this->input->post('jumlah'),
       );
       $this->db->where('NoPesanan',$NoPesanan);
       $this->db->update('kki_pemesanan',$data);
    }

    public function edit_pesanan($NoPesanan) {
        $config['upload_path'] = './assets/buktinota';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '10000';
        $config['max_width']  = '';
        $config['max_height']  = '';
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('buktinota'))
		{

            $file_name_gambar = $_FILES["buktinota"]["name"];
            $file_nama_gambar_md5= md5($file_name_gambar) ;
            $file_ext_gambar = pathinfo($file_nama_gambar_md5, PATHINFO_EXTENSION);
            $file_gambar = str_replace(' ', '_', $file_name_gambar).'.'.strtolower($file_ext_gambar);
            
		}
		else
		{
			echo $this->upload->display_errors();die();
		}

        $NoPesanan = $this->input->post('NoPesanan');
        $data = array (
            "TanggalPesanan" => $this->input->post('tgl_pemesanan'),
            "KodeSupplier" => $this->input->post('supplier'),
            "KodeBarang" => $this->input->post('barang'),
            "JumlahBarang" => $this->input->post('jumlah'),
            "buktinota" => $file_gambar
        );
        $this->db->where('NoPesanan',$NoPesanan);
        $this->db->update('kki_pemesanan',$data);
    }





    public function delete_pesanan($NoPesanan) {
        $this->db->where('NoPesanan',$NoPesanan);
        $this->db->delete('kki_pemesanan');
    }
    
    // DATA SUPPLIER
    public function get_listsupplier(){
        return $this->db->get('kki_supplier')->result_array();
    }
    public function add_listsupplier() {
        $data = array (
            "KodeSupplier" => $this->input->post('id'),
            "NamaSupplier" => $this->input->post('nama_supplier')
        );
        $this->db->insert('kki_supplier',$data);
    }
    public function edit_supplier($KodeSupplier) {
       $KodeSupplier = $this->input->post('KodeSupplier');
       $data = array (
        "NamaSupplier" => $this->input->post('NamaSupplier')
       );
       $this->db->where('KodeSupplier',$KodeSupplier);
       $this->db->update('kki_supplier',$data);
    }
    public function delete_supplier($KodeSupplier) {
        $this->db->where('KodeSupplier',$KodeSupplier);
        $this->db->delete('kki_supplier');
    }    

    // DATA BARANG
    public function get_listbarang(){
        return $this->db->get('kki_barang')->result_array();
    }
    public function add_barang() {
        $data = array (
            "KodeBarang" => $this->input->post('id'),
            "NamaBarang" => $this->input->post('nama_barang')
        );
        $this->db->insert('kki_barang',$data);
    }
    public function edit_barang($KodeBarang) {
       $KodeBarang = $this->input->post('KodeBarang');
       $data = array (
        "NamaBarang" => $this->input->post('NamaBarang')
       );
       $this->db->where('KodeBarang',$KodeBarang);
       $this->db->update('kki_barang',$data);
    }
    public function delete_barang($KodeBarang) {
        $this->db->where('KodeBarang',$KodeBarang);
        $this->db->delete('kki_barang');
    }

    //JOIN TABEL

    function getdatasupplier() {
        $this->db->select('kki_supplier.KodeSupplier,kki_supplier.NamaSupplier');
        $this->db->from('kki_supplier');      
        $query = $this->db->get();
        return $query;
     }
     function getdatabarang() {
        $this->db->select('kki_barang.KodeBarang,kki_barang.NamaBarang');
        $this->db->from('kki_barang');      
        $query = $this->db->get();
        return $query;
     }
    function joinsupplier() {
        $this->db->select('*');
        $this->db->from('kki_pemesanan');
        $this->db->join('kki_supplier','kki_pemesanan.KodeSupplier = kki_supplier.KodeSupplier');      
        $query = $this->db->get();
        return $query;
     }

     function joinpsnsubbrg() {
        $this->db->select('*');
        $this->db->from('kki_pemesanan');
        $this->db->join('kki_supplier','kki_pemesanan.KodeSupplier = kki_supplier.KodeSupplier','LEFT');      
        $this->db->join('kki_barang','kki_pemesanan.KodeBarang = kki_barang.KodeBarang','LEFT');
        $query = $this->db->get();
        return $query;
     }

    function cari_id($date)
    {
        $this->db->select_max('id_registrasi')
                    ->from('learning_pendaftaran')
                    ->like('id_registrasi',$date,'both');
        return $this->db->get();
    }
    
    
    
    
}
?>