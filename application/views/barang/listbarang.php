<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>List Barang - SIPBPT Niza Agen</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url()?>adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url()?>adminlte/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <!-- <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo base_url()?>" class="nav-link">Beranda</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Menu apa ya</a>
      </li> -->
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <!-- <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div> -->
    </form>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo base_url()?>list-pemesanan" class="brand-link">
      <img src="<?php echo base_url()?>adminlte/dist/img/AdminLTELogo.png"
           alt="Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Niza Agen</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo base_url()?>adminlte/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $this->session->userdata('nama_lengkap'); ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="<?php echo base_url()?>list-pemesanan" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <!-- <i class="right fas fa-angle-left"></i> -->
              </p>
            </a>            
          </li>

          <li class="nav-item has-treeview">
            <a href="<?php echo base_url()?>list-supplier" class="nav-link">
              <i class="far fa-edit nav-icon"></i>
              <p>
                List Supplier
                <!-- <i class="right fas fa-angle-left"></i> -->
              </p>
            </a>            
          </li>

          <li class="nav-item has-treeview">
            <a href="<?php echo base_url()?>list-barang" class="nav-link active">
              <i class="fas fa-book nav-icon"></i>
              <p>
                List Barang
                <!-- <i class="right fas fa-angle-left"></i> -->
              </p>
            </a>            
          </li>

          
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>


<?php
date_default_timezone_set('Asia/Jakarta');
$tglsekarang = date('Y/m/d/ h:i:s'); 
?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>List Data Barang</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="list-pemesanan">Beranda</a></li>
              <li class="breadcrumb-item active">List Data Barang</li>
            </ol>
          </div>
        </div>

        <?php if( $this->session->flashdata('msg') ) : ?>
								<div class="row mt-3">
									<div class="col-md-8">
										<div class="alert alert-success alert-dismissible fade show" role="alert">
											Data Pembelian Tunai <strong>berhasil</strong>  <?php $this->session->flashdata('msg'); ?>
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
									</div>
								</div>

							<?php endif; ?>



      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        
        <!-- /.row -->
        <div class="row">
          
        </div>
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <!-- <h3 class="card-title">Tabel Data</h3> -->

                <a class="btn btn-primary btn-sm text-right text-white" data-toggle="modal" data-target="#modal_add_new" >
                              <i class="fas fa-plus">
                              </i>
                              Add
                          </a> 
                          <!-- <a class="btn btn-info text-white" data-toggle="modal" data-target="#modal_add_new" ><i class="fa fa-user-plus"></i> &nbspTambah Pembelian Tunai</a> -->

              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0" style="height: 500px;">
                <table class="table table-head-fixed text-nowrap">
                  <thead>
                    <tr>
                      <th style="text-align:center;">Kode Barang</th>
                      <th style="text-align:center;">Nama Barang</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($data as $d) { ?>
                    <tr>
                      <td style="text-align:center;"><?= $d['KodeBarang']; ?></td>
                      <td style="text-align:center;"><?= $d['NamaBarang']; ?></td>
                      <td class="project-actions text-left">
                          <!-- <a class="btn btn-primary btn-sm" href="#">
                              <i class="fas fa-folder">
                              </i>
                              View
                          </a> -->
                          <a class="btn btn-outline-info btn-sm tampilModalUbah " data-toggle="modal" data-target="#modal_ubah<?= $d['KodeBarang']; ?>">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a class="btn btn-danger btn-sm" href="<?= base_url(); ?>c_home/deleteBarang/<?= $d['KodeBarang']; ?>" onclick="return confirm('Hapus data barang?');">
                              <i class="fas fa-trash">
                              </i>
                              Hapus
                          </a>

                      </td>
                    </tr>   
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>

        <!-- ============ Modal Tambah  =============== -->
        <div class="modal fade" id="modal_add_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<h3 class="modal-title" id="myModalLabel">Tambah Data Barang</h3>
												<button type="btn" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
											</div>
											<form class="form-horizontal" method="post" action="<?php echo base_url().'c_home/addBarang'?>">
												<div class="modal-body">


													<div class="form-group" hidden>
														<label class="control-label col-xs-3" >Kode Barang </label>
														<div class="col-xs-8">
															<input name="id" class="form-control" type="text" readonly required>
														</div>
                          </div>
                          
                          <div class="form-group">
														<label class="control-label col-xs-3" >Nama Barang</label>
														<div class="col-xs-8">
															<input name="nama_barang" class="form-control" type="text" placeholder="Petruk Beras/Aqua Air Minum.." required>
														</div>
													</div>												
													
												</div>

												<div class="modal-footer">

													<button class="btn btn-info">Simpan</button>
													<button class="btn btn-danger text-white" type="reset">Reset </button>

												</div>
											</form>
										</div>
									</div>
								</div>
                <!--END MODAL ADD -->
                
                <!-- ============ Modal Edit  =============== -->
								<?php foreach ($data as $data) : ?>
									<div class="modal fade" id="modal_ubah<?= $data['KodeBarang'];?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<h3 class="modal-title" id="myModalLabel">Edit Data Barang</h3>
													<button type="btn" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
												</div>
												<form class="form-horizontal" method="post" action="<?php echo base_url().'c_home/editBarang'?>">
													<div class="modal-body">

                            <div class="form-group">
														<label class="control-label col-xs-3" >Kode Barang</label>
														<div class="col-xs-8">
															<input value= "<?= $data['KodeBarang'];?>" name="KodeBarang" class="form-control" type="text" readonly required>
														</div>
													</div>

														<div class="form-group">
															<label class="control-label col-xs-3" >Nama Barang</label>
															<div class="col-xs-8">
																<input value="<?= $data['NamaBarang'];?>" name="NamaBarang" class="form-control" type="text" required>
															</div>
														</div>
                            
                            <!-- <div class="form-group">
														<label class="control-label col-xs-3" >Jumlah</label>
														<div class="col-xs-8">
															<input value= "<?= $data['JumlahBarang'];?>" name="jumlah" class="form-control" type="text" placeholder="Jumlah dalam angka" required>
														</div>
													</div> -->


													</div>

													<div class="modal-footer">

														<button class="btn btn-info">Simpan</button>
														<button class="btn btn-danger text-white" data-dismiss="modal" aria-hidden="true">Batal </button>

													</div>
												</form>
											</div>
										</div>
									</div>
								<?php endforeach; ?>
								<!--END MODAL EDIT ADMIN-->



        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>



  <!-- /.content-wrapper -->
  <footer class="main-footer">
    
    <strong>Copyright &copy; 2020 <a href="#">KKI-28</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url()?>adminlte/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url()?>adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url()?>adminlte/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url()?>adminlte/dist/js/demo.js"></script>
</body>
</html>
