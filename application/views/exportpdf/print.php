<html>
<head>
  <title>Cetak PDF</title>
  <style>
    table {
      border-collapse:collapse;
      table-layout:fixed;width: 630px;
    }
    table td {
      word-wrap:break-word;
      width: 20%;
    }
  </style>
</head>
<body>
    <b>NIZA AGEN</b><br />
    <b><?php echo $ket; ?></b><br /><br />
    
    <table border="1" cellpadding="8">
    <tr>
        <th style="text-align:center;">No Pemesanan</th>
        <th style="text-align:center;">Tanggal Pemesanan</th>
        <th style="text-align:center;">Supplier</th>
        <th style="text-align:center;">Barang</th>
        <th style="text-align:center;">Jumlah</th>
    </tr>
    <?php
    if( ! empty($transaksi)){
    	$no = 1;
    	foreach($transaksi as $data){
            $tgl = date('d-m-Y', strtotime($data->TanggalPesanan));
            
            echo "<tr>";
              echo "<td style='text-align:center;'>".$data->NoPesanan."</td>";
              echo "<td style='text-align:center;'>".$tgl."</td>";
              echo "<td style='text-align:center;'>".$data->NamaSupplier."</td>";
              echo "<td style='text-align:center;'>".$data->NamaBarang."</td>";
              echo "<td style='text-align:center;'>".$data->JumlahBarang." unit"."</td>";
    		echo "</tr>";
    		$no++;
    	}
    }
    ?>
  </table>
</body>
</html>