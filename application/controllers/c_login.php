<?php 
class C_login extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('login_model');
		$this->load->library('session');
	}


	public function auth(){
		$username = $this->input->post('username', TRUE);
		// $password = md5($this->input->post('password', TRUE));
		$password = $this->input->post('password', TRUE);
		$validasi = $this->login_model->validasi($username,$password);
		if($validasi->num_rows() > 0){
			$data = $validasi->row_array();
			$user_id = $data['user_id'];
			$username = $data['username'];
			$nama_lengkap = $data['nama_lengkap'];
			$rules = $data['rules'];
			$sessdata = array(
				'user_id' => $user_id,
				'username' => $username,
				'nama_lengkap' => $nama_lengkap,
				'rules' => $rules,
				'Logged_in' => TRUE
			);
			$this->session->set_userdata($sessdata);
			if($rules === '1'){
				redirect('list-pemesanan');
			}elseif($rules === '2'){
				redirect('list-pemesanan');
			}
		}
		else { echo $this->session->set_flashdata('pesan', 'username atau password salah!');
			redirect('c_home');

		}

	}


	function logout(){
		$this->session->sess_destroy();
		redirect('index');
	}





}


?>
