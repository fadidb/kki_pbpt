<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_home extends CI_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->helper('url');
		$this->load->model('kki_model');		
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index(){
		$this->load->view('login/login');

	}

	// BAGIAN PEMESANAN
	public function listPesanan() {
		if($this->session->userdata('Logged_in') === TRUE ){

			// $data['data'] 					= $this->kki_model->get_listpemesanan();
			// $data['join_pemesanan_supplier'] 	= $this->kki_model->joinsupplier()->result();
			$data['join_psn_sup_brg'] 	= $this->kki_model->joinpsnsubbrg()->result();
			$data['getdatasupplier'] 	= $this->kki_model->getdatasupplier()->result();
			$data['getdatabarang'] 	= $this->kki_model->getdatabarang()->result();
			
			// $data['join_psn_sup'] 	= $this->kki_model->joinsupplier()->result();
			// $data['join_pemesanan_supplier'] = $this->kki_model->joinsupplier();  
			
			// $this->load->view('includes/header');
			// $this->load->view('includes/sidebar');
			$this->load->view('pemesanan/home',$data);
			// $this->load->view('includes/footer');
		} else {
			redirect('c_home');
		}
	}
	public function addPesanan() {
		if($this->session->userdata('Logged_in') === TRUE ){ 
			$this->kki_model->add_listpemesanan2();
			$this->session->set_flashdata('msg', 'ditambahkan!');
			redirect('c_home/listPesanan');
		} else {
			redirect ('c_home');
		}
	}
	public function editPesanan($NoPesanan) {
		if ($this->session->userdata('Logged_in') === TRUE ){
			$this->kki_model->edit_pesanan($NoPesanan);
			$this->session->set_flashdata('msg','berhasil diubah!');
			redirect('c_home/listPesanan');
		} else {
			redirect('c_home');
		}
	}	
	public function deletePesanan($NoPesanan) {
		if ($this->session->userdata('Logged_in') === TRUE ){
			$this->kki_model->delete_pesanan($NoPesanan);
			$this->session->set_flashdata('msg','berhasil dihapus!');
			redirect('c_home/listPesanan');
		} else {
			redirect('c_home');
		}
	}

	// BAGIAN SUPPLIER
	public function list_supplier() {
		if ($this->session->userdata('Logged_in') === TRUE ){
			$data['data'] = $this->kki_model->get_listsupplier();
			$this->load->view('supplier/listsupplier',$data);
		} else {
			redirect('c_home');
		}
	}
	public function addSupplier() {
		if($this->session->userdata('Logged_in') === TRUE ){ 
			$this->kki_model->add_listsupplier();
			$this->session->set_flashdata('msg', 'ditambahkan!');
			redirect('c_home/list_supplier');
		} else {
			redirect ('c_home');
		}
	}
	
	public function editSupplier($KodeSupplier) {
		if ($this->session->userdata('Logged_in') === TRUE ){
			$this->kki_model->edit_supplier($KodeSupplier);
			$this->session->set_flashdata('msg','berhasil diubah!');
			redirect('c_home/list_supplier');
		} else {
			redirect('c_home');
		}
	}	
	public function deleteSupplier($KodeSupplier) {
		if ($this->session->userdata('Logged_in') === TRUE ){
			$this->kki_model->delete_supplier($KodeSupplier);
			$this->session->set_flashdata('msg','berhasil dihapus!');
			redirect('c_home/list_supplier');
		} else {
			redirect('c_home');
		}
	}


	// BAGIAN BARANG
	public function list_barang() {
		if ($this->session->userdata('Logged_in') === TRUE ){
			$data['data'] = $this->kki_model->get_listbarang();
			$this->load->view('barang/listbarang',$data);
		} else {
			redirect('c_home');
		}
	}
	public function addBarang() {
		if($this->session->userdata('Logged_in') === TRUE ){ 
			$this->kki_model->add_barang();
			$this->session->set_flashdata('msg', 'ditambahkan!');
			redirect('c_home/list_barang');
		} else {
			redirect ('c_home');
		}
	}
	
	public function editBarang($KodeBarang) {
		if ($this->session->userdata('Logged_in') === TRUE ){
			$this->kki_model->edit_barang($KodeBarang);
			$this->session->set_flashdata('msg','berhasil diubah!');
			redirect('c_home/list_barang');
		} else {
			redirect('c_home');
		}
	}	
	public function deleteBarang($KodeBarang) {
		if ($this->session->userdata('Logged_in') === TRUE ){
			$this->kki_model->delete_barang($KodeBarang);
			$this->session->set_flashdata('msg','berhasil dihapus!');
			redirect('c_home/list_barang');
		} else {
			redirect('c_home');
		}
	}

	//JOIN TABEL
	// public function data_anggota(){
	// 	$data['join_pemesanan_supplier'] = $this->kki_model->joinsupplier()->result(); 
	// 	// $this->load->view('anggota/anggotaview',$data);
	//  } 

	public function proses()
	{		
		$str_id = 'IDB-';
		$date = date('ymd');
		$id_registrasi =  $this->M_daftarbeasiswa->cari_id($date);
			
			foreach ( $id_registrasi->result() as $idregis )
			{
				$id_regisfix = substr($idregis->id_registrasi,-4);
			}
			if (!empty($id_regisfix)) {
				$last_id = (int)$id_regisfix;
			} else {
				$last_id = 0;
				// echo "error";
				// die();
			}
			$inc_id =  sprintf('%04d', ++$last_id);
			//  echo "inc_id".$inc_id;
			//  die();

			$new_id = $str_id . $date . $inc_id;
			// echo "id barunya ". $new_id;
			// die();
    }

    
	 

} 
?>