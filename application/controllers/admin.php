<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller{

	public function __construct(){
		parent:: __construct();
		$this->load->helper('url');
		$this->load->model('admin_model');
		$this->load->model('menu_model');
		date_default_timezone_set('Asia/Jakarta');

	}

	public function index(){
		$this->load->view('login/login');

    }
    
}
    ?>