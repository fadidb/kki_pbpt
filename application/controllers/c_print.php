<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_print extends CI_Controller {
  
  public function __construct(){
    parent::__construct();
    
    $this->load->model('print_model');
  }
  
  public function index(){
        if(isset($_GET['filter']) && ! empty($_GET['filter'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
            $filter = $_GET['filter']; // Ambil data filder yang dipilih user

            if($filter == '1'){ // Jika filter nya 1 (per tanggal)
                $tgl = $_GET['tanggal'];
                
                $ket = 'Data Pembelian Tunai Tanggal '.date('d-m-y', strtotime($tgl));
                $url_cetak = 'c_print/cetak?filter=1&tanggal='.$tgl;
                $transaksi = $this->print_model->view_by_date($tgl); // Panggil fungsi view_by_date yang ada di print_model
            }else if($filter == '2'){ // Jika filter nya 2 (per bulan)
                $bulan = $_GET['bulan'];
                $tahun = $_GET['tahun'];
                $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                
                $ket = 'Data Pembelian Tunai Bulan '.$nama_bulan[$bulan].' '.$tahun;
                $url_cetak = 'c_print/cetak?filter=2&bulan='.$bulan.'&tahun='.$tahun;
                $transaksi = $this->print_model->view_by_month($bulan, $tahun); // Panggil fungsi view_by_month yang ada di print_model
            }else{ // Jika filter nya 3 (per tahun)
                $tahun = $_GET['tahun'];
                
                $ket = 'Data Pembelian Tunai Tahun '.$tahun;
                $url_cetak = 'c_print/cetak?filter=3&tahun='.$tahun;
                $transaksi = $this->print_model->view_by_year($tahun); // Panggil fungsi view_by_year yang ada di print_model
            }
        }else{ // Jika user tidak mengklik tombol tampilkan
            $ket = 'Semua Data Pembelian Tunai';
            $url_cetak = 'c_print/cetak';
            $transaksi = $this->print_model->view_all(); // Panggil fungsi view_all yang ada di print_model
        }

    
        
    $data['ket'] = $ket;
    $data['url_cetak'] = base_url('index.php/'.$url_cetak);
    $data['transaksi'] = $transaksi;
        $data['option_tahun'] = $this->print_model->option_tahun();
    $this->load->view('exportpdf/view', $data);
  }
  
  public function cetak(){
        if(isset($_GET['filter']) && ! empty($_GET['filter'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
            $filter = $_GET['filter']; // Ambil data filder yang dipilih user

            if($filter == '1'){ // Jika filter nya 1 (per tanggal)
                $tgl = $_GET['tanggal'];
                
                $ket = 'Data Pembelian Tunai Tanggal '.date('d-m-y', strtotime($tgl));
                $transaksi = $this->print_model->view_by_date($tgl); // Panggil fungsi view_by_date yang ada di print_model
            }else if($filter == '2'){ // Jika filter nya 2 (per bulan)
                $bulan = $_GET['bulan'];
                $tahun = $_GET['tahun'];
                $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                
                $ket = 'Data Pembelian Tunai Bulan '.$nama_bulan[$bulan].' '.$tahun;
                $transaksi = $this->print_model->view_by_month($bulan, $tahun); // Panggil fungsi view_by_month yang ada di print_model
            }else{ // Jika filter nya 3 (per tahun)
                $tahun = $_GET['tahun'];
                
                $ket = 'Data Pembelian Tunai Tahun '.$tahun;
                $transaksi = $this->print_model->view_by_year($tahun); // Panggil fungsi view_by_year yang ada di print_model
            }
        }else{ // Jika user tidak mengklik tombol tampilkan
            $ket = 'Semua Data Pembelian Tunai';
            $transaksi = $this->print_model->view_all(); // Panggil fungsi view_all yang ada di print_model
        }
        
        $data['ket'] = $ket;
        $data['transaksi'] = $transaksi;
        
    ob_start();
    $this->load->view('exportpdf/print', $data);
    $html = ob_get_contents();
        ob_end_clean();
        
    require 'assets/html2pdf/autoload.php'; // Load plugin html2pdfnya
    $pdf = new Spipu\Html2Pdf\Html2Pdf('P','A4','en');  // Settingan PDFnya
    $pdf->WriteHTML($html);
    $pdf->Output('Data Pembelian Tunai.pdf', 'D');
  }
}